using System;
using RekenMachine;
using Xunit;

namespace RekenMachineTest
{
    public class RekenmachineTests
    {
        [Theory]
        [InlineData(4, 3, 7)]
        [InlineData(21, 5.25, 26.25)]
        [InlineData(double.MaxValue, 5, double.MaxValue)]
        public void Add_SimpleValuesShouldCalculate(double x, double y, double expected)
        {
            // Arrange

            // Act
            double actual = Calculator.Add(x, y);

            // Assert
            Assert.Equal(expected, actual);
        }

        /*[Theory]
        [InlineData(8, 4, 4)]
        [InlineData(double.MinValue, 5, double.MinValue)]
        public void Subtract_SimpleValuesShouldCalculate(double x, double y, double expected)
        {
            // Arrange

            // Act
            double actual = Calculator.Subtract(x, y);

            // Assert
            //Assert.Equal(expected, expected);
        } */

    }
}